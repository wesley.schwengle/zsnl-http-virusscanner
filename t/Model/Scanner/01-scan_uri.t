use Test::Most;

use Test::MockObject;
use Test::MockModule;

use URI;
my $URI = URI->new('https://foo');

use_ok('Zaaksysteem::VirusScanner::Model::Scanner');

use Net::ClamAV::Exception::Other;

my %mocked = ();

$mocked{'Zaaksysteem::VirusScanner::Model::Scanner'} =
    _mocked_zaaksysteem_virusscanner_model_scanner('Hello World');

subtest "Default attributes" => sub {

    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new();
    isa_ok( $virus_scanner, 'Zaaksysteem::VirusScanner::Model::Scanner',
        "New virus_scanner without constructor attributes"
    );
    can_ok( $virus_scanner, 'scan_uri' );

    can_ok( $virus_scanner->scanner_client, 'ping' );
    can_ok( $virus_scanner->scanner_client, 'scanStreamFile' );
    can_ok( $virus_scanner->user_agent, 'get' );

    throws_ok (
        sub {
            $virus_scanner->scan_uri( undef );
        }, qr/failed: Validation failed for 'URI'/,
        "... requires URI"
    )

};

subtest "No Scanner" => sub {

    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new(
        scanner_client => _mocked_scanner_no_ping(),
    );

    my @viruses;
    throws_ok (
        sub {
            @viruses = $virus_scanner->scan_uri($URI);
        }, 'Net::ClamAV::Exception::Other',
        "... throws error when there is no scanner"
    );

};

subtest "Scanner throws errors" => sub {

    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new(
        scanner_client => _mocked_scanner_errors(),
    );

    my @viruses;
    throws_ok (
        sub {
            @viruses = $virus_scanner->scan_uri($URI);
        }, 'Net::ClamAV::Exception::Other',
        "Throws error when there is an internal error"
    );
};

subtest "Virus Found" => sub {

    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new(
        scanner_client => _mocked_scanner_virus_found(),
        user_agent     => _mocked_user_agent_with_response(),
    );

    my @viruses;
    lives_ok ( 
        sub {
            @viruses = $virus_scanner->scan_uri($URI);
        }, "Virus Scanner -> scan_uri"
    );

    cmp_deeply ( \@viruses,
        [ '*** VIRUS ***' ],
        "... found 1 expected virus"
    );
};

subtest "Clean" => sub {

    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new(
        scanner_client => _mocked_scanner_virus_clean(),
        user_agent     => _mocked_user_agent_with_response(),
    );

    my @viruses;
    lives_ok ( 
        sub {
            @viruses = $virus_scanner->scan_uri($URI);
        }, "Virus Scanner -> scan_uri"
    );
    cmp_deeply ( \@viruses,
        [ ],
        "... found empty list of viruses"
    );
};

done_testing();

# _mocked_zaaksysteem_virusscanner_model_scanner($data)
#
# mock private _file_temp_from_uri so we do not fire-off LWP::UserAgent
# - return a FH for a File::Temp, with $data
#
sub _mocked_zaaksysteem_virusscanner_model_scanner {
    my $data = shift;
    my $mock = Test::MockModule->new('Zaaksysteem::VirusScanner::Model::Scanner');
    $mock->mock(
        '_file_temp_from_uri' => sub {
            my ($self, $uri ) = @_;
            my $temp_file = File::Temp->new();
            print $temp_file $data;
            return $temp_file;
        }
    );
    return $mock;
}

sub _mocked_scanner_no_ping {
    my $mock = Test::MockObject->new();

    # ClamAV::Client->scan_stream shouldn't be possible, can't ping
    $mock->mock(
        'ping' => sub {
            throw Net::ClamAV::Exception::Other();
        },
        'scanStreamFile' => sub {
            die "you shouldn't be here\n"
        }
    );

    return $mock;
}

sub _mocked_scanner_errors {
    my $mock = Test::MockObject->new();
    $mock->set_true('ping');
    # Net::ClamAV::Client->scanStreamFile can throw errors
    $mock->mock(
        'scanStreamFile' => sub {
            throw Net::ClamAV::Exception::Other();
        }
    );

    return $mock;
}

sub _mocked_scanner_virus_found {
    my $mock = Test::MockObject->new();
    $mock->set_true('ping');
    # ClamAV::Client->scan_stream returns 1 string for a virus that it detects
    $mock->set_always( 'scanStreamFile' => '*** VIRUS ***' );

    return $mock;
}

sub _mocked_scanner_virus_clean {
    my $mock = Test::MockObject->new();
    $mock->set_true('ping');
    # CLamAV::Client->scan_stream returns undef if no virus are detected
    $mock->set_always( 'scanStreamFile' => undef );

    return $mock;
}

# hoo hooo hooo .... we need data in temp_file
sub _mocked_user_agent_with_response {
    my $response = shift;
    my $mock = Test::MockObject->new();
    $mock->mock('get' => $response );

    return $mock;
}

1;
