use Test::Most;

use Test::MockModule;
use Test::MockObject;

use BTTW::Tools;

use Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore;

use HTTP::Response;
use HTTP::Request::JSON;
use Readonly;

Readonly my $UUID => 'dddf07d7-b8e2-4008-b6da-bd89bb77d673';

my $mock_model_scanner = Test::MockModule->new('Zaaksysteem::VirusScanner::Model::Scanner');
# dispatch instantiates it new Zaaksysteem::VirusScanner::Model::Scanner object
# we need to mock the behaviour of that newly created $virus_scanner

my $request_handler =
    Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
        service => _mocked_service_http(),
    );

subtest "Gets the right config" => sub {
    cmp_deeply ( $request_handler->_config,
        {
            download_template => 'file://xt/file/{file_id}/download',
            callback_template => 'file://xt/file/{file_id}/virus-scan-status',
            platform_key      => 'test_key',
        },
        "when using the '_build__config'"
    )
};

subtest "Virus Scanner can't find configuration" => sub {
    my $user_agent = _mocked__user_agent__ok();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http__no_config(),
        );
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request() );
        }, qr(virus_scanner_service/filestore/misconfiguration),
        "Throws: Virus Scanner can't find configuration"
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Virus Scanner missing download_template" => sub {
    my $user_agent = _mocked__user_agent__ok();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http__no_config__download_template(),
        );
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request() );
        }, qr(virus_scanner_service/filestore/misconfiguration/download_template),
        "Throws: Virus Scanner missing download_template"
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Virus Scanner missing callback_template" => sub {
    my $user_agent = _mocked__user_agent__ok();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http__no_config__callback_template(),
        );
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request() );
        }, qr(virus_scanner_service/filestore/misconfiguration/callback_template),
        "Throws: Virus Scanner missing callback_template"
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Virus Scanner missing platform_key" => sub {
    my $user_agent = _mocked__user_agent__ok();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http__no_config__platform_key(),
        );
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request() );
        }, qr(virus_scanner_service/filestore/misconfiguration/platform_key),
        "Throws: Virus Scanner missing platform_key"
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Virus clean" => sub {
    my $user_agent = _mocked__user_agent__ok();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http(),
            _user_agent => $user_agent,
        );
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    my $result;
    lives_ok (
        sub { 
            $result = $request_handler->dispatch( _mocked_request() )
        }, "Did dispatch for 'Virus Clean'"
    );

    $user_agent->called_ok('request');

    my ($name,$args) = $user_agent->next_call();
    my $request = $args->[1];
    cmp_deeply( $request->json_content => 
        {
            virus_scan_status => 'ok',
            viruses => bag ( )
        },
        "... and did send an empty list of viruses"
    );
# TODO: { local $TODO = "VSS could return JSON serialized syzygy object";
#     cmp_deeply ( $result,
#         {
#             scan_uri     => 'https://test.com/file.txt',
#             scan_result  => 'OK',
#             scan_viruses => bag( ),
#         },
#         "OK - no viruses found"
#     );
# } # TODO

};

subtest "Virus found" => sub {
    my $user_agent = _mocked__user_agent__ok();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http(),
            _user_agent => $user_agent,
        );
    $mock_model_scanner->mock( 'new' =>  _mocked_model_scanner__found() );

    my $result;

    lives_ok (
        sub { 
            $result = $request_handler->dispatch( _mocked_request() )
        }, "Did dispatch for 'Virus Found'"
    );

    $user_agent->called_ok('request');

    my ($name,$args) = $user_agent->next_call();
    my $request = $args->[1];
    cmp_deeply( $request->json_content => 
        {
            virus_scan_status => 'found',
            viruses => bag ('*** VIRUS ***','*** TERROR ***')
        },
        "... and did send the right list of viruses"
    );

# TODO: { local $TODO = "VSS could return JSON serialized syzygy object";
#     cmp_deeply ( $result,
#         {
#             scan_uri     => 'https://test.com/file.txt',
#             scan_result  => 'FOUND',
#             scan_viruses => bag(
#                 '*** VIRUS ***',
#                 '*** TERROR ***',
#             ),
#         },
#         "FOUND - two viruses"
#     );
# } # TODO

};

subtest "Dispatch Request: missing arguments" => sub {
    my $user_agent = _mocked__user_agent__ok();
    $mock_model_scanner->mock('new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request__no_file_id() );
        }, qr(params/profile),
        "Throws: Virus Scanner needs a UUID to scan"
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Dispatch Request: wrong arguments" => sub {
    my $user_agent = _mocked__user_agent__ok();
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request__bad_file_id() );
        }, qr(params/profile),
        "Throws: Virus Scanner not a valid UUID ..."
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Dispatch: throws errors" => sub {
    my $user_agent = _mocked__user_agent__ok();
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__errors() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request() );
        }, qr(virus_scanner_service/scanner/test_exception),
        "Throws: Virus Scan Service causing TEST exception"
    );

    ok !$user_agent->called('request'), "... and did not make a callback";

};

subtest "Dispatch: throws errors" => sub {
    my $user_agent = _mocked__user_agent__errors();
    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => _mocked_service_http(),
            _user_agent => $user_agent,
        );
    $mock_model_scanner->mock( 'new' => _mocked_model_scanner__clean() );

    throws_ok(
        sub {
            $request_handler->dispatch( _mocked_request() );
        }, qr(virus_scanner_service/filestore/callback_failed),
        "Throws: Virus Scan Service causing TEST exception"
    );

    ok $user_agent->called('request'),
    "... because it did try to make a callback that was meant to fail";

};

done_testing;

sub _mocked_service_http {
    my $mock = Test::MockObject->new();
    $mock->set_isa('Zaaksysteem::Service::HTTP');
    $mock->set_always('service_config' => _service_config() );
    $mock->set_always('set_log_context' => 1);
    return $mock;
}

sub _mocked_service_http__no_config {
    my $mock = _mocked_service_http();
    $mock->service_config->{'Zaaksysteem::Service::HTTP'} = undef;
    return $mock;
}

sub _mocked_service_http__no_config__download_template {
    my $mock = _mocked_service_http();
    delete $mock->service_config->{'Zaaksysteem::Service::HTTP'}->{download_template};
    return $mock;
}

sub _mocked_service_http__no_config__callback_template {
    my $mock = _mocked_service_http();
    delete $mock->service_config->{'Zaaksysteem::Service::HTTP'}->{callback_template};
    return $mock;
}

sub _mocked_service_http__no_config__platform_key {
    my $mock = _mocked_service_http();
    delete $mock->service_config->{'Zaaksysteem::Service::HTTP'}->{platform_key};
    return $mock;
}

# a full set of valid body params, delete or alter when testing exceptions
sub _request_body_params {
    return Hash::MultiValue->new(
        file_id => $UUID,
        instance_hostname => 'localhost'
    );
}

sub _service_config {
    return +{
        'Zaaksysteem::Service::HTTP' => {
            download_template => 'file://xt/file/{file_id}/download',
            callback_template => 'file://xt/file/{file_id}/virus-scan-status',
            platform_key      => 'test_key',
        }
    }
}

sub _mocked_request {
    my $body_params = _request_body_params();

    my $mock = Test::MockObject->new();
    $mock->set_always('body_parameters' => $body_params );
    return $mock;
}

sub _mocked_request__no_file_id {
    my $body_params = _request_body_params();
    delete $body_params->{file_id};

    my $mock = Test::MockObject->new();
    $mock->set_always('body_parameters' => $body_params );
    return $mock;
}

sub _mocked_request__bad_file_id {
    my $body_params = _request_body_params();
    $body_params->{file_id} = "this-is-not-a-valid-uuid";

    my $mock = Test::MockObject->new();
    $mock->set_always('body_parameters' => $body_params );
    return $mock;
}

sub _mocked_model_scanner__found {
    my $user_agent = shift;
    my $mock = Test::MockObject->new();
    $mock->mock(
        'user_agent' => sub { return $user_agent }
    );
    $mock->set_list( 'scan_uri' => '*** VIRUS ***', '*** TERROR ***' );
    return $mock;
}

sub _mocked_model_scanner__clean {
    my $user_agent = shift;
    my $mock = Test::MockObject->new();
    $mock->mock(
        'user_agent' => sub { return $user_agent }
    );
    $mock->mock(
        'scan_uri' => sub {
            my ($self, $uri) = @_;
            is $uri, "file://xt/file/${UUID}/download",
                "got the right URI";
            return ();
        }
    );
    return $mock;
}

sub _mocked_model_scanner__errors {
    my $user_agent = shift;
    my $mock = Test::MockObject->new();
    $mock->mock(
        'user_agent' => sub { return $user_agent }
    );
    $mock->mock(
        'scan_uri' => sub {
            throw (
                'virus_scanner_service/scanner/test_exception',
                "Virus Scan Service causing TEST exception",
            )
        }
    );
    return $mock;
}

sub _mocked__user_agent__ok {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_always( 'request' => HTTP::Response->new("200") );
    return $mock_obj;
}

sub _mocked__user_agent__errors {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_always( 'request' => HTTP::Response->new("500") );
    return $mock_obj;
}

1;
