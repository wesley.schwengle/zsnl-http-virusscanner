package Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore;

use Moose;

use Syzygy::Object::Model;

with qw[
    Zaaksysteem::Service::HTTP::RequestHandler
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore - virusscan integration

=head1 DESCRIPTION

Request Handler that will take the uuid body param and C<dispatch> the forward
the request to the  L<Zaaksysteem::VirusScanner::Model::Scanner>->C<scan_uri>.

<dispatch> will make a callback-request to Zaaksysteem and report the list of
found viruses - hopefully an empty list to indicate that no virusses have been
detected, that means, it's OK.

When everything works fine, downloading the file, scanning for viruses and
making the callback to Zaaksysteem, the C<dispatch> will report a HTTP-Status of
200 (OK).

Many problems will be caught and exceptions will be thrown, resulting in a HTTP-
status code 500.

=head1 SYNOPSIS

    my $request_handler =
        Zaaksysteem::VirusScanner::Service::RequestHandlers::Filestore->new(
            service => $service_http,
        );
    
    my $result = $request_handler->dispatch( $request );

=cut

use BTTW::Tools;
use BTTW::Tools::UA;
use Syzygy::Types qw[UUID];
use Zaaksysteem::VirusScanner::Model::Scanner;

use HTTP::Request::JSON;
use Try::Tiny;
use URI::Template;
use UUID::Tiny;

has _config => (
    is => 'ro',
    lazy => 1,
    builder => '_build__config'
    
);

sub _build__config {
    my $self = shift;
    my $config = $self->service->service_config->{ 'Zaaksysteem::Service::HTTP' } or throw (
        'virus_scanner_service/filestore/misconfiguration',
        "Virus Scanner can't find configuration"
    );
    return  return $config;
}

has _user_agent => (
    is => 'ro',
    lazy => 1,
    builder => '_build__user_agent',
);

sub _build__user_agent {
    my ($self) = @_;

    my $user_agent = LWP::UserAgent->new(
        ssl_opts => { verify_hostname => 0 },
        agent => 'Zaaksysteem-Virus-Scan-Service/0.1b',
        protocols_allowed => [qw[http https]]
    );

    $user_agent->default_header( 'ZS-Platform-Key' => $self->_platform_key );

    return $user_agent;
}



=head1 METHODS

=head2 name

Implements the name interface required by
L<Zaaksysteem::Service::HTTP::RequestHandler>.

Returns the empty string, this handler attaches to the root of the
application.

=cut

sub name { 'filestore' }

=head2 dispatch

Implements the C<dispatch> interface
required by L<Zaaksysteem::Service::HTTP::RequestHandler>.

The $request expects to have the following body parameters:

=over

=item file_id

UUID reference to the file to be scanned for viruses.

=item instance_hostname

Hostname of the instance where the file is located.

=back

=cut

define_profile dispatch => (
    required => {
        file_id => UUID,
        instance_hostname => 'Str',
    },
    optional => {
        mode => 'Str',
    },
    defaults => {
        mode => 'async',
    },
);

sub dispatch {
    my ($self, $request, $response) = @_;

    my $params = assert_profile(
        $request->body_parameters->as_hashref
    )->valid;

    my $file_id = $params->{ file_id };
    my $host = $params->{ instance_hostname };

    $self->service->set_log_context(file_id => $file_id);
    $self->service->set_log_context(instance_hostname => $host);

    $self->log->info(sprintf(
        'Starting scan for file "%s" on instance "%s"',
        $file_id,
        $host
    ));

    my $uri_download = $self->_uri_for_filestore_download($host, $file_id);

    my $virus_scanner = Zaaksysteem::VirusScanner::Model::Scanner->new(
        user_agent => $self->_user_agent,
    );

    my @viruses = $virus_scanner->scan_uri($uri_download);

    my $virus_found = scalar(@viruses) > 0;

    if ( scalar(@viruses) ) {
        $self->log->info(sprintf('FOUND virus in "%s": %s',
            "$uri_download",
            join( ', ', @viruses ),
        ));
    } else {
        $self->log->info(sprintf('OK for "%s"', "$uri_download"));
    }

    # we are done here when scanning in ASYNC mode
    return Syzygy::Object::Model->new_object(scanner_result => {viruses => \@viruses})
        if $params->{ mode } eq 'sync';

    my $uri_callback = $self->_uri_for_filestore_callback($host, $file_id);
    my $callback_request = HTTP::Request::JSON->new( POST => "$uri_callback" );

    $callback_request->json_content({
        virus_scan_status => (scalar @viruses ? 'found' : 'ok'),
        viruses => \@viruses
    });

    $self->log->info(sprintf(
        'Requesting update of file scan status for file "%s" on instance "%s"',
        UUID::Tiny::uuid_to_string($file_id),
        $host
    ));

    my $callback_response = $self->_user_agent->request($callback_request);

    # We assume that any 200-range status code implies successful processing
    # of the status update.
    return if $callback_response->is_success;

    my $status_code = $callback_response->code();
    my $status_message = HTTP::Status::status_message($status_code);

    throw(
        'virus_scanner_service/filestore/callback_failed',
        sprintf("Virus Scan Service callback failed: [%s - http status: %s: %s]",
            $uri_callback->as_string(),
            $status_code,
            $status_message,
        )
    );

    # nope, we do not get here
    return;

}

sub _uri_for_filestore_download {
    my ( $self, $instance_hostname, $file_id ) = @_;

    my $template = URI::Template->new($self->_filestore_download_template());
    my $uri = $template->process(
        instance_hostname => $instance_hostname,
        file_id => $file_id
    );

    return $uri;
}

sub _filestore_download_template {
    my $self = shift;
    my $value = $self->_config->{download_template} or throw (
        'virus_scanner_service/filestore/misconfiguration/download_template',
        "Virus Scanner missing download_template"
    );
    return $value;
}

sub _uri_for_filestore_callback {
    my ( $self, $instance_hostname, $file_id ) = @_;

    my $template = URI::Template->new($self->_filestore_callback_template());
    my $uri = $template->process(
        instance_hostname => $instance_hostname,
        file_id => $file_id
    );

    return $uri;
}

sub _filestore_callback_template {
    my $self = shift;
    my $value = $self->_config->{callback_template} or throw (
        'virus_scanner_service/filestore/misconfiguration/callback_template',
        "Virus Scanner missing callback_template"
    );
    return $value;
}

sub _platform_key {
    my $self = shift;
    my $value = $self->_config->{platform_key} or throw (
        'virus_scanner_service/filestore/misconfiguration/platform_key',
        "Virus Scanner missing platform_key"
    );
    return $value;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the
C<CONTRIBUTORS> file.

Zaaksysteem::VirusScanner uses the EUPL license, for more information please
have a look at the C<LICENSE> file.

